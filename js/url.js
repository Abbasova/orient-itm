$(document).ready(function() {
    let urlArray = document.URL.split('/');
    let lastURL = urlArray[urlArray.length - 1];
    console.log(lastURL);
    if(lastURL === "mentor.html") lastURL = "mentors.html";
    if(lastURL === "training.html") lastURL = "trainings.html";

    let navbar = document.getElementsByClassName('navbar-nav')[0];
    let items = navbar.getElementsByClassName('nav-item');
    for( let i = 0; i < items.length; i++) {
        if(items[i].getElementsByClassName('nav-link')[0].getAttribute('href') === lastURL )  
        items[i].classList.add('active');
    }
    $('.navbar-toggler').on('click', function() {
        if( $(this).hasClass('collapsed')) 
            $('body').css('overflow', 'hidden');
        else 
            $('body').css('overflow', 'auto');
    });
});