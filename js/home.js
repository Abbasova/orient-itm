let servicesBtnState = false;

$(document).ready( function() {
    $('.navbar-toggler').on('click', function() {
        if($(this).hasClass('collapsed'))
            $('.navbar').css('height', $(window).height()); 
        else 
            $('.navbar').css('height', 100); 
    });
    let n = 0;  
    $('.hidden-services').hide();
    $('.show-hidden-services').on('click', function() {
        n = 0;
        $('.hidden-services').slideToggle();
        servicesBtnState = !servicesBtnState;
        if(servicesBtnState)
            $('.show-hidden-services').html('<i class="fas fa-arrow-up"></i> XİDMƏTLƏRİ GİZLƏ');
        else 
            $('.show-hidden-services').html('<i class="fas fa-arrow-down"></i> BÜTÜN XİDMƏTLƏRƏ BAX');
    });
    if($('select').length !== 0)
        $('select').formSelect();   
    let showDeatails = function() {
        let cards = document.getElementsByClassName('services-card');
        if(getComputedStyle(document.getElementsByClassName('hidden-services')[0], null).display === "none" ) {
            for(let i = 0; i < cards.length; i++) {
                cards[i].getElementsByClassName('card-body')[0].style.opacity = "0";
            }
            cards[n].getElementsByClassName('card-body')[0].style.opacity = "1";
            n++;
            if(n === 4) n = 0;
        }
        else {
            for(let i = 0; i < cards.length; i++) {
                cards[i].getElementsByClassName('card-body')[0].style.opacity = "0";
            }
            if(n === 0)
                cards[cards.length - 1].getElementsByClassName('card-body')[0].style.opacity = "0";
            else
                cards[n - 1].getElementsByClassName('card-body')[0].style.opacity = "0";
            cards[n].getElementsByClassName('card-body')[0].style.opacity = "1";
            n++;
            if(n === cards.length) n = 0;
        }
    }
    setInterval(showDeatails, 1000);  
    $('.feedbacks-slick-container').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        infinite: true,
        prevArrow: false,
        nextArrow: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 5000,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
                },
                {
                    breakpoint: 1200,
                    settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.navbar-toggler').on('click', function() {
        if($('.navbar-toggler').hasClass('collapsed')) {
            $('.navbar').addClass('navbar-bg');
        }
        else {
            $('.navbar').removeClass('navbar-bg');
        }
    });
});
// $(window).resize(function() {
//     $('.feedbacks-slick-container').slick({
//         autoplay: true,
//         autoplaySpeed: 2000,
//         dots: true,
//         infinite: true,
//         prevArrow: false,
//         nextArrow: false,
//         slidesToShow: 3,
//         slidesToScroll: 3,
//         responsive: [
//             {
//                 breakpoint: 5000,
//                 settings: {
//                     slidesToShow: 2,
//                     slidesToScroll: 2,
//                 }
//                 },
//                 {
//                     breakpoint: 1200,
//                     settings: {
//                     slidesToShow: 2,
//                     slidesToScroll: 2
//                     }
//                 },
//                 {
//                     breakpoint: 768,
//                     settings: {
//                     slidesToShow: 1,
//                     slidesToScroll: 1
//                 }
//             }
//         ]
//     });
// });